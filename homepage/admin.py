# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Friend, ClassYear
admin.site.register(Friend)
admin.site.register(ClassYear)