# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'Homepage.html')

def message(request):
    return render(request, 'Message.html')

def home(request):
    return render(request, 'Homepage.html')